package com.arakon.S7;

import com.arakon.S7.configuration.AuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class S7Application {

    @Autowired
    AuthenticationFilter authenticationFilter;

    public static void main(String[] args) {
        SpringApplication.run(S7Application.class, args);
    }

    @Bean
    public FilterRegistrationBean authenticationFilterRegistration() {
        final FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(authenticationFilter);
        filterRegistrationBean.addUrlPatterns("/events/single/*");
        filterRegistrationBean.addUrlPatterns("/comments/*");
        filterRegistrationBean.addUrlPatterns("/participants/*");
        return filterRegistrationBean;
    }

    @Bean
    public WebMvcConfigurerAdapter forwardToIndex() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addViewControllers(ViewControllerRegistry registry) {
                // forward requests to "/" to "index.html"
                registry.addViewController("/").setViewName(
                        "/index.html");
            }
        };
    }
}
