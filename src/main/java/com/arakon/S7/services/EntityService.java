package com.arakon.S7.services;

import com.arakon.S7.configuration.AuthenticationFilter;
import com.arakon.S7.entities.Event;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.ArrayList;
import java.util.Date;

import static org.springframework.web.context.request.RequestAttributes.SCOPE_REQUEST;

@Service
public class EntityService {


    @Value("${anonymous.name}")
    private String ANONYMOUS_NAME;
    @Value("${anonymous.picture}")
    private String ANON_PIC;

    public AuthenticationFilter.UserInfo getUserAttribute() {
        return (AuthenticationFilter.UserInfo) RequestContextHolder.getRequestAttributes().getAttribute(AuthenticationFilter.USER, SCOPE_REQUEST);
    }

    public Event processAnonymity(Event event) {
        final AuthenticationFilter.UserInfo userInfo = getUserAttribute();
        if (event.getIsAnonymous() && !event.getUserId().equals(getUserInfoId(userInfo))) {
            event.setPicture(ANON_PIC);
            event.setUserId(null);
            event.setUserName(ANONYMOUS_NAME);
        }
        event.getParticipants().forEach(p -> {
            if (p.getIsAnonymous() && !p.getUserId().equals(getUserInfoId(userInfo))) {
                p.setUserId(null);
                p.setName(ANONYMOUS_NAME);
                p.setPicture(ANON_PIC);
            }
        });
        event.getComments().forEach(c -> {
            if (c.getIsAnonymous() && !c.getAuthorId().equals(getUserInfoId(userInfo))) {
                c.setPicture(ANON_PIC);
                c.setAuthorName(ANONYMOUS_NAME);
                c.setAuthorId(null);
            }
        });
        return event;
    }

    public Iterable<Event> getEventBriefs(Iterable<Object[]> rows) {
        final ArrayList<Event> result = new ArrayList<>();
        rows.forEach(row -> {
            final Event event = new Event();
            event.setId((Long) row[0]);
            event.setLatitude((Double) row[1]);
            event.setLongitude((Double) row[2]);
            event.setCategory((String) row[3]);
            event.setStartTime((Date) row[4]);
            event.setEndTime((Date) row[5]);
            event.setTitle((String) row[6]);
            result.add(event);
        });
        return result;
    }

    private String getUserInfoId(AuthenticationFilter.UserInfo userInfo) {
        return userInfo == null ? null : userInfo.getId();
    }
}
