package com.arakon.S7.services;

import com.arakon.S7.configuration.AuthenticationFilter;
import com.arakon.S7.entities.Comment;
import com.arakon.S7.entities.Event;
import com.arakon.S7.entities.Participant;
import com.arakon.S7.repositories.CommentRepository;
import com.arakon.S7.repositories.EventRepository;
import com.arakon.S7.repositories.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Set;

@Service
public class PersistenceService {

    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private ParticipantRepository participantRepository;
    @Autowired
    private EntityService entityService;

    public boolean saveComment(long eventId, Comment comment) {
        final Event event = eventRepository.findOne(eventId);
        if (event != null) {
            final AuthenticationFilter.UserInfo userInfo
                    = entityService.getUserAttribute();
            comment.setAuthorName(userInfo.getName());
            comment.setAuthorId(userInfo.getId());
            comment.setTime(new Date());
            comment.setPicture(userInfo.getPicture());
            commentRepository.save(comment);
            event.getComments().add(comment);
            eventRepository.save(event);
            return true;
        }
        return false;
    }

    public boolean saveParticipant(long eventId, boolean isAnonymous) {
        final Event event = eventRepository.findOne(eventId);
        if (event != null) {
            final AuthenticationFilter.UserInfo userInfo = entityService.getUserAttribute();
            Participant participant = new Participant(userInfo, isAnonymous);
            participantRepository.save(participant);
            event.getParticipants().add(participant);
            eventRepository.save(event);
            return true;
        }

        return false;
    }

    public void delete(long eventId) {
        eventRepository.delete(eventId);
    }

    public Iterable<Event> findAllEvents() {
        return entityService.getEventBriefs(eventRepository.getAllBrief());
    }

    public Event findOneEvent(long eventId) {
        return entityService.processAnonymity(eventRepository.findOne(eventId));
    }

    public Event saveEvent(Event event) {
        final AuthenticationFilter.UserInfo userInfo = entityService.getUserAttribute();
        event.setUserId(userInfo.getId());
        event.setUserName(userInfo.getName());
        event.setPicture(userInfo.getPicture());
        event.setEventTime(new Date());
        return eventRepository.save(event);
    }

    // Temporary not in use
    public Set<Participant> findAllParticipants(long eventId) {
        final Event event = eventRepository.findOne(eventId);
        return event == null
                ? null
                : event.getParticipants();
    }

    public boolean deleteParticipant(long eventId, String userId) {
        final Event event = eventRepository.findOne(eventId);
        final boolean result =
                event != null && event.getParticipants().removeIf(
                        p -> p.getUserId() == userId
                );
        eventRepository.save(event);
        return result;
    }

    public boolean removeComment(long eventId, long id) {
        final Event event = eventRepository.findOne(eventId);
        final boolean result
                = event != null && event.getComments().removeIf(comment -> comment.getCommentId() == id);
        eventRepository.save(event);
        return result;
    }
}
