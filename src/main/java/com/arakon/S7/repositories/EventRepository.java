package com.arakon.S7.repositories;

import com.arakon.S7.entities.Event;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface EventRepository extends CrudRepository<Event, Long> {

    @Query("SELECT e.id, e.latitude, e.longitude, e.category, e.startTime, e.endTime, e.title FROM Event e")
    Iterable<Object[]> getAllBrief();
}
