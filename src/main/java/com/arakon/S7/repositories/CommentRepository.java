package com.arakon.S7.repositories;

import com.arakon.S7.entities.Comment;
import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<Comment, Long> {
}
