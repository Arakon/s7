package com.arakon.S7.repositories;

import com.arakon.S7.entities.Participant;
import org.springframework.data.repository.CrudRepository;

public interface ParticipantRepository extends CrudRepository<Participant, String> {
}
