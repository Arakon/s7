package com.arakon.S7.configuration;

import lombok.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpMethod;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Log4j2
public class AuthenticationFilter extends OncePerRequestFilter {

    // TODO move this out of here
    public static final String USER = "user_info";
    private static final int UNAUTHORIZED = HttpServletResponse.SC_UNAUTHORIZED;
    private static final String GET = HttpMethod.GET.name();
    private static final String OPTIONS = HttpMethod.OPTIONS.name();
    private static final String AUTHORIZATION = "Authorization";
    private static String[] fields = {"id", "email", "first_name", "last_name"};

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            if (!OPTIONS.equals(request.getMethod())) {
                final String token = request.getHeader(AUTHORIZATION);
                if (StringUtils.isEmpty(token) && !GET.equals(request.getMethod())) {
                    response.sendError(UNAUTHORIZED);
                } else if (StringUtils.isEmpty(token) && GET.equals(request.getMethod())) {
                    filterChain.doFilter(request, response);
                } else {
                    final Facebook facebook = new FacebookTemplate(token);
                    final User userProfile = facebook.fetchObject("me", User.class, fields);
                    final String id = userProfile.getId();
                    if (!StringUtils.isEmpty(id)) {
                        final UserInfo userInfo = new UserInfo(
                                userProfile.getFirstName() + " " + userProfile.getLastName(),
                                "http://graph.facebook.com/" + id + "/picture?type=square",
                                id
                        );
                        request.setAttribute(USER, userInfo);
                        filterChain.doFilter(request, response);
                    } else {
                        response.sendError(UNAUTHORIZED);
                    }
                }
            } else {
                filterChain.doFilter(request, response);
            }
        } catch (Exception e) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    @AllArgsConstructor
    @Getter
    @Setter
    public static class UserInfo {
        private final String name;
        private final String picture;
        private final String id;
    }

}
