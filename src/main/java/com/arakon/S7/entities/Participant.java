package com.arakon.S7.entities;

import com.arakon.S7.configuration.AuthenticationFilter;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
public class Participant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long participantId;
    private String userId;
    private String name;
    private String picture;
    private Boolean isAnonymous;

    public Participant(AuthenticationFilter.UserInfo userInfo, boolean isAnonymous) {
        this.setName(userInfo.getName());
        this.setUserId(userInfo.getId());
        this.setPicture(userInfo.getPicture());
        this.setIsAnonymous(isAnonymous);
    }
}
