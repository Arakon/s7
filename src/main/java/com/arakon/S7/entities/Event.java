package com.arakon.S7.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Data
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String userId;
    private double latitude;
    private double longitude;
    private Date startTime;
    private Date endTime;
    private Date eventTime;
    @Column(length = 5000)
    private String message;
    private String title;
    private String userName;
    private String picture;
    private String category;
    private Boolean isAnonymous;
    @Transient
    private Integer participantsCount;
    @Transient
    private Integer commentsCount;
    @OneToMany(fetch=FetchType.LAZY)
    private Set<Participant> participants;
    @OneToMany(fetch=FetchType.LAZY)
    private Set<Comment> comments;

    @PostLoad
    private void postLoad() {
        participantsCount = participants.size();
        commentsCount = comments.size();
    }
}
