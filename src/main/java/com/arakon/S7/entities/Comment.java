package com.arakon.S7.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long commentId;
    private String authorId;
    private Date time;
    @Column(length = 5000)
    private String message;
    private String authorName;
    private String picture;
    private Boolean isAnonymous;
}
