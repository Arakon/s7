package com.arakon.S7.controllers;

import com.arakon.S7.entities.Comment;
import com.arakon.S7.services.PersistenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/comments")
@CrossOrigin
public class CommentController {

    private final PersistenceService persistenceService;

    @Autowired()
    CommentController(PersistenceService repository) {
        this.persistenceService = repository;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{eventId}")
    boolean addComment(@RequestBody Comment comment, @PathVariable long eventId) {
        return persistenceService.saveComment(eventId, comment);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{eventId}")
    boolean removeComment(@RequestBody Comment comment, @PathVariable long eventId) {
        return persistenceService.removeComment(eventId, comment.getCommentId());
    }
}
