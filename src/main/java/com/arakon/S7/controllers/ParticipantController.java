package com.arakon.S7.controllers;

import com.arakon.S7.services.PersistenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/participants")
@CrossOrigin
public class ParticipantController {

    private final PersistenceService persistenceService;

    @Autowired()
    ParticipantController(PersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{eventId}")
    boolean addParticipant(@PathVariable long eventId, @RequestBody boolean isAnonymous) {
        return persistenceService.saveParticipant(eventId, isAnonymous);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{participantId}")
    void removeParticipant(@PathVariable String userId, @RequestBody long eventId) {
        persistenceService.deleteParticipant(eventId, userId);
    }

}
