package com.arakon.S7.controllers;

import com.arakon.S7.entities.Event;
import com.arakon.S7.services.PersistenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/events")
@CrossOrigin
public class EventController {

    @Autowired
    private PersistenceService persistenceService;

    @RequestMapping(method=RequestMethod.GET)
    Iterable<Event> getAllEvents() {
        return persistenceService.findAllEvents();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/single/{eventId}")
    Event getEvent(@PathVariable long eventId) {
        return persistenceService.findOneEvent(eventId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/single")
    ResponseEntity<Long> addEvent(@RequestBody Event input) {
        final Event event = persistenceService.saveEvent(input);
        final URI uri = ServletUriComponentsBuilder
                .fromCurrentRequestUri()
                .pathSegment(event.getId() + "")
                .build().toUri();
        final ResponseEntity.BodyBuilder created = ResponseEntity.created(uri);
        created.header("ID", event.getId() + "");
        return created.body(event.getId());
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/single/{eventId}")
    void removeEvent(@PathVariable long eventId) {
        persistenceService.delete(eventId);
    }
}
