-- DROP SEQUENCE public.comment_comment_id_seq;

CREATE SEQUENCE public.comment_comment_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.event_id_seq;

CREATE SEQUENCE public.event_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

-- DROP SEQUENCE public.participant_participant_id_seq;

CREATE SEQUENCE public.participant_participant_id_seq
INCREMENT BY 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1;

CREATE TABLE public."comment" (
	comment_id int8 NOT NULL DEFAULT nextval('comment_comment_id_seq'::regclass),
	author_id varchar(255) NULL,
	author_name varchar(255) NULL,
	is_anonymous bool NULL,
	message varchar(5000) NULL,
	picture varchar(255) NULL,
	"time" timestamp NULL,
	CONSTRAINT comment_pkey PRIMARY KEY (comment_id)
)
WITH (
OIDS=FALSE
) ;

CREATE TABLE public.participant (
	participant_id int8 NOT NULL DEFAULT nextval('participant_participant_id_seq'::regclass),
	is_anonymous bool NULL,
	"name" varchar(255) NULL,
	picture varchar(255) NULL,
	user_id varchar(255) NULL,
	CONSTRAINT participant_pkey PRIMARY KEY (participant_id)
)
WITH (
OIDS=FALSE
) ;

CREATE TABLE public.event (
	id int8 NOT NULL DEFAULT nextval('event_id_seq'::regclass),
	category varchar(255) NULL,
	end_time timestamp NULL,
	event_time timestamp NULL,
	is_anonymous bool NULL,
	latitude float8 NOT NULL,
	longitude float8 NOT NULL,
	message varchar(5000) NULL,
	picture varchar(255) NULL,
	start_time timestamp NULL,
	title varchar(255) NULL,
	user_id varchar(255) NULL,
	user_name varchar(255) NULL,
	CONSTRAINT event_pkey PRIMARY KEY (id)
)
WITH (
OIDS=FALSE
) ;

CREATE TABLE public.event_comments (
	event_id int8 NOT NULL,
	comments_comment_id int8 NOT NULL,
	CONSTRAINT event_comments_pkey PRIMARY KEY (event_id,comments_comment_id),
	CONSTRAINT uk_et614qxaxhxa32pt9v6w2ycr0 UNIQUE (comments_comment_id),
	CONSTRAINT fkbg5m1v1ibc2tlum0pk3p1agfu FOREIGN KEY (event_id) REFERENCES public.event(id),
	CONSTRAINT fkrkgd7h3nqly05ei1yfododfbn FOREIGN KEY (comments_comment_id) REFERENCES public."comment"(comment_id)
)
WITH (
OIDS=FALSE
) ;

CREATE TABLE public.event_participants (
	event_id int8 NOT NULL,
	participants_participant_id int8 NOT NULL,
	CONSTRAINT event_participants_pkey PRIMARY KEY (event_id,participants_participant_id),
	CONSTRAINT uk_nwvxd7b9nnjml7kpcqk49pyhq UNIQUE (participants_participant_id),
	CONSTRAINT fk5232w1ta0icpkemgsxyw8a976 FOREIGN KEY (event_id) REFERENCES public.event(id),
	CONSTRAINT fkcsxws7iqewv06sxm419i0mpet FOREIGN KEY (participants_participant_id) REFERENCES public.participant(participant_id)
)
WITH (
OIDS=FALSE
) ;
