package com.arakon.S7.integration;

import com.arakon.S7.entities.Comment;
import com.arakon.S7.entities.Event;
import com.arakon.S7.repositories.EventRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CommentControllerIntegrationTest {
    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private EventRepository eventRepository;
    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        final Event e1 = new Event();
        e1.setName("event1");
        final List<Comment> comments = new ArrayList<>();
        comments.add(new Comment());
        comments.add(new Comment());
        comments.add(new Comment());
        comments.add(new Comment());
        e1.setComments(comments);
        eventRepository.save(e1);
    }

    @Test
    public void testGetAll() throws Exception {
        final ResultActions result = mockMvc
                .perform(get("/comments/3"));
        result.andExpect(status().isOk());
//        result.andExpect(content().contentType(contentType));
    }

    @Test
    public void testAddComment() throws Exception {
        final String testString = "{\n" +
                "\"message\": \"message1\"\n" +
                "}";

        mockMvc
                .perform(post("/comments/3")
                        .contentType(contentType)
                        .content(testString))
                .andExpect(status().isCreated());
    }
}
