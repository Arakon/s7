package com.arakon.S7.integration;

import com.arakon.S7.entities.Event;
import com.arakon.S7.entities.Participant;
import com.arakon.S7.repositories.EventRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ParticipantControllerIntegrationTests {

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private EventRepository eventRepository;
    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        final Event e1 = new Event();
        e1.setName("event1");
        List<Participant> participants = new ArrayList<>();
        participants.add(new Participant());
        participants.add(new Participant());
        participants.add(new Participant());
        participants.add(new Participant());
        e1.setParticipants(participants);
        eventRepository.save(e1);
    }

    @Test
    public void testGetAll() throws Exception {
        mockMvc
                .perform(get("/participants/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType));
    }

    @Test
    public void testAddParticipant() throws Exception {
        final String testString = "{\n" +
                "\"name\": \"participant2\"\n" +
                "}";

        final MvcResult mvcResult = mockMvc
                .perform(post("/participants")
                        .contentType(contentType)
                        .content(testString))
                .andExpect(status().isCreated())
                .andReturn();
        final boolean matches = mvcResult.getResponse()
                .getHeader("Location")
                .matches("http://localhost/participants/\\d+");
        assertTrue(matches);
    }

    @Test
    public void testRemoveParticipant() throws Exception {
        mockMvc
                .perform(delete("/participants/0"))
                .andExpect(status().isOk());
    }
}
