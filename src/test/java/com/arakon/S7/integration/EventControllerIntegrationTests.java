package com.arakon.S7.integration;

import com.arakon.S7.entities.Event;
import com.arakon.S7.repositories.EventRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


@RunWith(SpringRunner.class)
@SpringBootTest
public class EventControllerIntegrationTests {

	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(),
			Charset.forName("utf8"));

	@Autowired
	private WebApplicationContext webApplicationContext;
	@Autowired
	private EventRepository eventRepository;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();
		final Event e1 = new Event();
		e1.setName("event1");
		final Event e2 = new Event();
		e2.setName("event2");
		final Event e3 = new Event();
		e3.setName("event3");
		eventRepository.save(e1);
		eventRepository.save(e2);
		eventRepository.save(e3);
	}

	@Test
	public void testGetAll() throws Exception {
		mockMvc
				.perform(get("/events/"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(contentType));
	}

	@Test
	public void testGetEvent() throws Exception {
		final Event e = new Event();
		e.setName("event4");
		final long id = eventRepository.save(e).getId();
		mockMvc
				.perform(get("/events/" + id))
				.andExpect(status().isOk())
				.andExpect(content().contentType(contentType));
	}

	@Test
	public void testAddEvent() throws Exception {
		final String testString = "{\n" +
				"\"name\": \"event2\"\n" +
				"}";

		final MvcResult mvcResult = mockMvc
				.perform(post("/events")
						.contentType(contentType)
						.content(testString))
				.andExpect(status().isCreated())
				.andReturn();
		final boolean matches = mvcResult.getResponse()
				.getHeader("Location")
				.matches("http://localhost/events/\\d+");
		assertTrue(matches);
	}

	@Test
	public void testRemoveEvent() throws Exception {
		final Event e = new Event();
		e.setName("event4");
		final long id = eventRepository.save(e).getId();
		mockMvc
				.perform(delete("/events/" + id))
				.andExpect(status().isOk());
	}
}
