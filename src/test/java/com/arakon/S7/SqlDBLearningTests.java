package com.arakon.S7;

import com.arakon.S7.entities.Event;
import com.arakon.S7.repositories.EventRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SqlDBLearningTests {

    @Autowired
    private EventRepository eventRepository;

    @Before
    public void setup() {
        eventRepository.deleteAll();
    }

    private Event createEvent(String name) {
        Event event = new Event();
        event.setName(name);
        return event;
    }

    @Test
    public void shouldCreate() {
        final Event save = eventRepository.save(createEvent("test0"));
        assertNotNull(save);
    }

    @Test
    public void shoultCreateWithIdGreaterThanZero() {
        eventRepository.save(createEvent("test1"));
        final Event save = eventRepository.save(createEvent("test2"));
        assertNotNull(save);
        assertTrue(save.getId() > 0);
    }
}
