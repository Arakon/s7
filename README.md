# S7

### BRIEF

**S7** is a codename of the backend for my project of events-on-the-map. The main idea is that user can create custom event markers on the map. Choose appropriate category for it, start/end time, etc. Of course anyone can view events nearby, join to each of them, put a comment, see other participants. You can pass authentication of the system, using popular social networks. Also there is an option to stay anonymous for another members. Own authorization system is not in plans. It is possible to filter out events by different criterions.

### TECHNOLOGIES

*   Spring Boot
*   PostgreSQL
*   Gradle
*   Project Lombok
*   IntelliJ IDEA
*   Others

### INSTALLATION

To run S7 on my ;) Ubuntu-machine follow the steps:

1.  Install and configure PostgreSQL. You should use <link>https://help.ubuntu.com/stable/serverguide/postgresql.html.
2.  Change password of the **postgres** user.
3.  Run `sudo -u postgres psql` and `CREATE DATABASE "S7"` to create S7 database. Double quotes are obligate.
4.  Run the project!